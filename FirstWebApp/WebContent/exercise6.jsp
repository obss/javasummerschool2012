<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<table border="1">
    <tr>
        <td colspan="2" align="center"><b>REQUEST PARAMETERS</b></td>
    </tr>
    <%
        Set<Entry<String,String[]>> params = request.getParameterMap().entrySet();
    for (Entry<String,String[]> en:params )    
        {

    %>
    <tr>
        <td><%=en.getKey()%>:</td>
        <td><%=en.getValue()[0]%></td>
    </tr>
    <%}%>
</table>

</body>
</html>