<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=ISO-8859-9" language="java" %>
<html>
<head><title>Sistem Özellikleri</title></head>
<body>
<table border="1">
    <tr>
        <td>Runtime.getRuntime().availableProcessors():</td>
        <td><%=Runtime.getRuntime().availableProcessors()%></td>
    </tr>
    <tr>
        <td>Runtime.getRuntime().totalMemory():</td>
        <td><%=Runtime.getRuntime().totalMemory()%>B &nbsp;&nbsp;&nbsp;<%=Runtime.getRuntime().totalMemory()/(1024*1024)%>MB</td>
    </tr>
    <tr>
        <td>Runtime.getRuntime().freeMemory():</td>
        <td><%=Runtime.getRuntime().freeMemory()%></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><b>SYSTEM PROPERTIES</b></td>
    </tr>
    <%
        Properties p = System.getProperties();
        Enumeration en=p.propertyNames();
        while(en.hasMoreElements())
        {
            String key=en.nextElement().toString();
            String value=p.getProperty(key);

    %>
    <tr>
        <td><%=key%>:</td>
        <td><%=value%></td>
    </tr>
    <%}%>
    <tr>
        <td colspan="2" align="center"><b>SYSTEM ENVIRONMENT</b></td>
    </tr>
    <%
        Map m = System.getenv();
        Set s = m.keySet();
        Iterator it = s.iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            String value = (String) m.get(key);

    %>
    <tr>
        <td><%=key%>:</td>
        <td><%=value%></td>
    </tr>
    <%}%>
</table>

</body>
</html>