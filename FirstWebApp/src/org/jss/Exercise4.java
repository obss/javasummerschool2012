package org.jss;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Exercise4
 */
@WebServlet("/Exercise4")
public class Exercise4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Exercise4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String html="<html>"+
						"<body style=\"color: red;\">"+
							"<table style=\"font-weight: bold;\" border=\"1\">"+
								"<tr><td>Hello world!</td></tr>"+
							"</table>"+
						"</body>"+
					"</html>";
		response.getWriter().write(html);
	}

}
